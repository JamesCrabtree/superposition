using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayWalkSound : MonoBehaviour
{
    AudioSource walkSound;

    public static PlayWalkSound Instance;
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        walkSound = GetComponent<AudioSource>();
    }

    public void Play()
    {
        if(!walkSound.isPlaying)
            walkSound.Play();
    }

    public void Pause()
    {
        if (walkSound.isPlaying)
            walkSound.Pause();
    }
}
