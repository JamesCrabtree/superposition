using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChange : MonoBehaviour
{
    private bool isOpen;

    public bool startsOpen;

    Animator animator;

    private void Start()
    {
        isOpen = startsOpen;
        animator = GetComponentInChildren<Animator>();
    }

    public void open()
    {
        isOpen = true;
        animator.SetBool("open", true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isOpen && collision.gameObject.CompareTag("Player"))
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
