using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pushable : MonoBehaviour
{
    Pushable replicatedPushable;
    Rigidbody2D rb;

    bool isBeingPushed;
    Vector2 lastPos;
    Vector2 movementVector;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Replicated replicated = GetComponent<Replicated>();
        if (replicated != null && replicated.isReplicated())
        {
            replicatedPushable = replicated.getOtherPushable();
        }

        isBeingPushed = false;
        lastPos = transform.position;
        movementVector = Vector2.zero;
    }

    void FixedUpdate()
    {
        if (replicatedPushable != null && replicatedPushable.BeingPushed)
            rb.MovePosition((Vector2) transform.position + replicatedPushable.MovementVector);
        if (isBeingPushed)
            movementVector = (Vector2) transform.position - lastPos;
        else
            movementVector = Vector2.zero;
        lastPos = transform.position;
    }

    public Vector2 MovementVector
    {
        get
        {
            return movementVector;
        }
    }

    public bool BeingPushed
    {
        get
        {
            return isBeingPushed;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
            isBeingPushed = true;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            StartCoroutine(handlePushStop());
    }

    private IEnumerator handlePushStop()
    {
        while (rb.velocity.magnitude > 0)
            yield return null;
        isBeingPushed = false;
    }
}
