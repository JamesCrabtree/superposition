using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using TMPro;
using System;


public class DeathScreenScript : MonoBehaviour
{
    Canvas deathScreenCanvas;

    public static bool InDeathScreen;
    public static bool QuittingGame;

    public static DeathScreenScript Instance;
    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        InDeathScreen = false;
        deathScreenCanvas = transform.GetChild(0).GetComponent<Canvas>();
        deathScreenCanvas.gameObject.SetActive(false);
        Time.timeScale = 1f;
    }

    public void OpenDeathMenu()
    {
        InDeathScreen = true;
        Time.timeScale = 0f;
        deathScreenCanvas.gameObject.SetActive(true);
    }

    public void TryAgain()
    {
        if (!InDeathScreen)
            return;

        ReloadLevel();
    }

    public void QuitGame()
    {
        if (!InDeathScreen)
            return;

        QuittingGame = true;

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
         Application.OpenURL(webplayerQuitURL);
#else
         Application.Quit();
#endif
    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}