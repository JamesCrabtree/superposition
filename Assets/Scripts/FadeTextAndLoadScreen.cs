using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using TMPro;
using System;

public class FadeTextAndLoadSceneScript : MonoBehaviour
{
    public CanvasGroup myCanvasGroup, line1, line2, line3, line4;

    public bool AllowInput { get; private set; }
    private const float fadeTime = 0.7f;
    private const float waitTime = 1.3f;

    public static FadeTextAndLoadSceneScript Instance;

    void Awake()
    {
        Instance = this;
    }

    public void DoFadeAndLoadScene()
    {
        StartCoroutine(fadeAndLoadSceneInternal());
    }

    IEnumerator fadeInCanvasGroup(CanvasGroup cg)
    {
        float startTime = Time.time;
        cg.alpha = 0f;
        while (Time.time - startTime < fadeTime)
        {
            cg.alpha = Mathf.Lerp(0f, 1f, (Time.time - startTime) / fadeTime);
            yield return null;
        }
        cg.alpha = 1f;
    }

    IEnumerator fadeAndLoadSceneInternal()
    {
        yield return StartCoroutine(fadeInCanvasGroup(myCanvasGroup));
        yield return new WaitForSeconds(waitTime);
        yield return StartCoroutine(fadeInCanvasGroup(line1));
        yield return new WaitForSeconds(waitTime);
        yield return StartCoroutine(fadeInCanvasGroup(line2));
        yield return new WaitForSeconds(waitTime);
        yield return StartCoroutine(fadeInCanvasGroup(line3));
        yield return new WaitForSeconds(waitTime);
        yield return StartCoroutine(fadeInCanvasGroup(line4));
        yield return new WaitForSeconds(waitTime);
        yield return new WaitForSeconds(waitTime);
        yield return new WaitForSeconds(waitTime);

        LoadScene();
    }

    private void LoadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
