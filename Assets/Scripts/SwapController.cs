using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapController : MonoBehaviour
{
    [HideInInspector] public Swappable PinkOwned, BlueOwned;

    public static SwapController Instance;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        
    }

    public void AssignObjectAndSwap(Swappable swappableObject, SwapBulletScript.ColorType bulletColor)
    {
        if (bulletColor == SwapBulletScript.ColorType.Pink)
        {
            PinkOwned = swappableObject;
            BlueOwned = null;
        }
        else
        {
            BlueOwned = swappableObject;
        }

        if (PinkOwned != null && BlueOwned != null)
        {
            ObjectSwapSounds.Instance.PlayRandomSound();
            PinkOwned.swap(BlueOwned);
            PinkOwned = null;
            BlueOwned = null;
        }

    }

}
