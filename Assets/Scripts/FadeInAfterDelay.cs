using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInAfterDelay : MonoBehaviour
{
    public float delay;
    public bool StartFullAlpha;

    CanvasGroup canvasGroup;
    private const float fadeTime = 0.9f;

    void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        if (StartFullAlpha)
            canvasGroup.alpha = 1;
        else
            StartCoroutine(fadeInAfterDelay());
    }

    IEnumerator fadeInAfterDelay()
    {
        canvasGroup.alpha = 0f;
        if (delay > 0f)
            yield return new WaitForSeconds(delay);

        float startTime = Time.time;
        while (Time.time - startTime < fadeTime)
        {
            canvasGroup.alpha = Mathf.Lerp(0f, 1f, (Time.time - startTime) / fadeTime);
            yield return null;
        }
        canvasGroup.alpha = 1f;
    }
}
