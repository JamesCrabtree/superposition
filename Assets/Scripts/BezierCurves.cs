using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves
{
    public class BezierCurves
    {
        public static Vector3 GetBezierPosition(Vector3 start, Vector3 end, Vector3 startDir, Vector3 endDir, float t)
        {
            Vector3 p0 = start;
            Vector3 p1 = p0 + startDir;
            Vector3 p3 = end;
            Vector3 p2 = p3 - endDir;

            // this is an equation for a cubic bezier curve
            return Mathf.Pow(1f - t, 3f) * p0 + 3f * Mathf.Pow(1f - t, 2f) * t * p1 + 3f * (1f - t) * Mathf.Pow(t, 2f) * p2 + Mathf.Pow(t, 3f) * p3;
        }

        public static Vector3 GetBezierPositionByP0123(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
        {
            // this is an equation for a cubic bezier curve
            return Mathf.Pow(1f - t, 3f) * p0 + 3f * Mathf.Pow(1f - t, 2f) * t * p1 + 3f * (1f - t) * Mathf.Pow(t, 2f) * p2 + Mathf.Pow(t, 3f) * p3;
        }

        public static bool BadVector(Vector3 v)
        {
            return float.IsNaN(v.x) || float.IsNaN(v.y) || float.IsNaN(v.z) ||
                float.IsInfinity(v.x) || float.IsInfinity(v.y) || float.IsInfinity(v.z);
        }

        public static bool BadVector(Vector2 v)
        {
            return float.IsNaN(v.x) || float.IsNaN(v.y) ||
                float.IsInfinity(v.x) || float.IsInfinity(v.y);
        }
    }
}