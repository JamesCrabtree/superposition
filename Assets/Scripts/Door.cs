using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    SpriteRenderer[] renderers;
    BoxCollider2D col;

    private bool isOpen;

    public bool startsOpen;

    // Start is called before the first frame update
    void Start()
    {
        renderers = GetComponentsInChildren<SpriteRenderer>();
        col = GetComponent<BoxCollider2D>();

        isOpen = startsOpen;
    }

    public void switchState() 
    {
        if (isOpen)
            close();
        else
            open();
    }

    private void open()
    {
        foreach(SpriteRenderer sr in renderers)
            sr.enabled = false;
        col.enabled = false;
        isOpen = true;
    }

    private void close()
    {
        foreach (SpriteRenderer sr in renderers)
            sr.enabled = true;
        col.enabled = true;
        isOpen = false;
    }
}
