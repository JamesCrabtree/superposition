using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Replicated : MonoBehaviour
{
    public Replicated other;

    public bool isReplicated()
    {
        return other != null;
    }

    public Swappable getOtherSwappable()
    {
        return other.GetComponent<Swappable>();
    }

    public Pushable getOtherPushable()
    {
        return other.GetComponent<Pushable>();
    }
}
