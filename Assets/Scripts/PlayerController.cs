using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;

public class PlayerController : MonoBehaviour
{

    [HideInInspector] public static GameObject Player;

    //public GameObject PlayerFlameParticlesPrefab;

    public Pivot.Dimension StartingDimension;

    Animator animator;
    Rigidbody2D rb;
    Transform terrainCollider;
    SpriteRenderer spriteRenderer;
    CameraShake cameraShake;
    PlayWalkSound playWalkSound;
    AudioSource walkSound, hurtBurnSound;

    public bool Alive { get; set; }

    private bool InExplosion = false;

    private Vector3 oldMovingDir;

    private const float constantWalkSpeed = 4.5f;

    public const int W_KEY_WINDOWS = 0x57;
    public const int A_KEY_WINDOWS = 0x41;
    public const int S_KEY_WINDOWS = 0x53;
    public const int D_KEY_WINDOWS = 0x44;
#if UNITY_EDITOR_WIN
    [DllImport("user32.dll")]
    public static extern short GetAsyncKeyState(int vkey);
#endif

    private Vector3 lastLegalPos;
    private float lastV, lastH;

    public static PlayerController Instance;

    void Awake()
    {
        Instance = this;
        Player = gameObject;
        Alive = true;
        Pivot.curDimension = StartingDimension;
    }

    public static void SetFrameSettingsAtStart()
    {
        QualitySettings.vSyncCount = 1;
        Application.targetFrameRate = Screen.currentResolution.refreshRate;
        Time.fixedDeltaTime = (1f / (1f * Screen.currentResolution.refreshRate));
        Time.maximumDeltaTime = (1f / 15f);
        Time.maximumParticleDeltaTime = (1f / 60f);
    }

    private void Start()
    {
        SetFrameSettingsAtStart();
        rb = GetComponent<Rigidbody2D>();
        //AudioSource[] sounds = GetComponents<AudioSource>();
        //walkSound = sounds[2];
        //hurtBurnSound = sounds[3];
        cameraShake = CameraShake.Instance;
        playWalkSound = PlayWalkSound.Instance;
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        //animator = transform.GetChild(0).GetComponent<Animator>();
        terrainCollider = transform.Find("Terrain Collider");
        lastLegalPos = transform.position;
        oldMovingDir = Vector3.zero;
        lastV = 0f;
        lastH = 0f;
        //walkSound.pitch = Random.Range(0.6f, 0.7f);
        //walkSound.volume = Random.Range(0.2f, 0.3f);
    }

    //public void Explosion(Vector2 bombPos)
    //{
    //    InExplosion = true;
    //    Vector2 dir = (rb.position - bombPos).normalized;
    //    rb.velocity = (dir * Random.Range(14f, 18f));
    //    GameObject newExplodeObject = Instantiate(PlayerFlameParticlesPrefab, transform.position, Quaternion.identity, transform);
    //    newExplodeObject.transform.localPosition = new Vector2(0f, -0.33f);
    //    ParticleSystem newParticles = newExplodeObject.GetComponent<ParticleSystem>();
    //    newParticles.Play();
    //    KillPlayer();
    //}

    public void KillPlayer()
    {
        StartCoroutine(killPlayerAfterDelay());
    }

    IEnumerator killPlayerAfterDelay()
    {
        yield return new WaitForSeconds(0.25f);

        hurtBurnSound.pitch = Random.Range(0.95f, 1.05f);
        hurtBurnSound.volume = Random.Range(0.7f, 0.8f);
        hurtBurnSound.Play();

        yield return new WaitForSeconds(0.15f);

        float startTime = Time.unscaledTime;
        const float duration = 1f;
        while (Time.unscaledTime - startTime < duration)
        {
            Time.timeScale = Mathf.Lerp(1f, 0f, (Time.unscaledTime - startTime) / duration);
            yield return null;
        }
        Time.timeScale = 0;
        Alive = false;
    }

    void Update()
    {
        if (!Alive)
        {
            if (!DeathScreenScript.InDeathScreen)
                DeathScreenScript.Instance.OpenDeathMenu();

            oldMovingDir = Vector3.zero;
            lastV = 0f;
            lastH = 0f;
            return;
        }

        if (!FadeSceneScript.Instance.AllowInput || InExplosion)
        {
            oldMovingDir = Vector3.zero;
            lastV = 0f;
            lastH = 0f;
            return;
        }

        if (Input.GetButtonDown("Reset"))
            DeathScreenScript.Instance.ReloadLevel();

        //if (Input.GetKeyDown(KeyCode.F))

        if (Input.GetKeyDown(KeyCode.Space))
            Pivot.CurDimensionPivot().SwitchDimensions();

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        //AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(0);

#if UNITY_EDITOR_WIN
        if (Mathf.Abs(h) < 0.1f)
        {
            if ((GetAsyncKeyState(D_KEY_WINDOWS) & 0x8000) > 0)
                h = 1f;
            else if ((GetAsyncKeyState(A_KEY_WINDOWS) & 0x8000) > 0)
                h = -1f;
        }
        if (Mathf.Abs(v) < 0.1f)
        {
            if ((GetAsyncKeyState(W_KEY_WINDOWS) & 0x8000) > 0)
                v = 1f;
            else if ((GetAsyncKeyState(S_KEY_WINDOWS) & 0x8000) > 0)
                v = -1f;
        }
#endif

        lastH = h;
        lastV = v;//record the h and v for walking here, use them in FixedUpdate() if not in dash

        HandleSpriteRotation(h, v);

        if (InputSpeed(h, v) > 0f)
            playWalkSound.Play();
        else
            playWalkSound.Pause();

        //HandleMovementSounds();
        //HandleSpriteSwitching(lastH, lastV);
    }

    private void FixedUpdate()
    {
        if (!InExplosion)
            HandleMovement(lastH, lastV);

        //if(legal)
        //    lastLegalPos = transform.position;
    }

    public bool PlayerNotMoving()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        return Mathf.Abs(h) < 0.1f && Mathf.Abs(v) < 0.1f;
    }

    void HandleMovementSounds()
    {
        if (PlayerNotMoving())
        {
            if (walkSound.isPlaying)
                walkSound.Pause();
        }
        else if (!walkSound.isPlaying)
        {
            walkSound.Play();
        }
    }

    public float playerCurrentWalkSpeed()
    {
        return constantWalkSpeed;
    }

    private static float InputSpeed(float h, float v)
    {
        Vector2 direction = (h * Vector2.right + v * Vector2.up).normalized;
        return Mathf.Max(Mathf.Abs(h), Mathf.Abs(v));
    }

    public void HandleMovement(float h, float v)
    {
        float speed = playerCurrentWalkSpeed();
        Vector2 direction = (h * Vector2.right + v * Vector2.up).normalized;
        float inputSpeed = Mathf.Max(Mathf.Abs(h), Mathf.Abs(v));
        //float height = heightController.CurrentHeightVelocity;
        if (direction.magnitude > 0f)
            oldMovingDir = direction;

        float curSpeed = speed * inputSpeed;
        rb.MovePosition(rb.position + (direction * curSpeed * Time.fixedDeltaTime));
    }

    private void HandleSpriteRotation(float h, float v)
    {
        if (ShootController.Instance.HoldingBullet)
        {
            Vector2 aimDir = AimController.Instance.AimDirection();
            float angle = Mathf.Atan2(aimDir.y, aimDir.x) * Mathf.Rad2Deg;
            spriteRenderer.transform.rotation = Quaternion.Lerp(spriteRenderer.transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), Time.deltaTime * 20f);
        }
        else if (InputSpeed(h, v) > 0f && oldMovingDir != Vector3.zero)
        {
            float angle = Mathf.Atan2(oldMovingDir.y, oldMovingDir.x) * Mathf.Rad2Deg;
            spriteRenderer.transform.rotation = Quaternion.Lerp(spriteRenderer.transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), Time.deltaTime * 7f);
        }
    }

    public Vector3 DirectionPlayerMovedLast
    {
        get
        {
            return oldMovingDir;
        }
    }
    public Vector3 LastLegalPos
    {
        get
        {
            return lastLegalPos;
        }
    }
}
