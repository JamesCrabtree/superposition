using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using TMPro;
using System;

public class FadeSceneScript : MonoBehaviour
{
    Canvas canvas;
    CanvasGroup canvasGroup;

    public bool AllowInput { get; private set; }
    private const float fadeInTime = 0.5f;
    private const float fadeOutTime = 1f;
    private bool fading = false;

    public static FadeSceneScript Instance;

    void Awake()
    {
        Instance = this;
        Transform fadeCanvasObject = transform.Find("Fade Canvas");
        canvas = fadeCanvasObject.GetComponent<Canvas>();
        canvasGroup = fadeCanvasObject.GetComponent<CanvasGroup>();
        StartCoroutine(fadeInAtStart());
    }

    IEnumerator fadeInAtStart()
    {
        AllowInput = false;
        fading = true;
        float startTime = Time.time;
        canvasGroup.alpha = 1f;
        canvas.enabled = true;
        while (Time.time - startTime < fadeInTime)
        {
            canvasGroup.alpha = Mathf.Lerp(1f, 0f, (Time.time - startTime) / fadeInTime);
            yield return null;
        }
        canvasGroup.alpha = 0f;
        canvas.enabled = false;

        fading = false;
        AllowInput = true;
    }



    public void FadeAndLoadNextScene()
    {
        if (!fading)
            StartCoroutine(fadeAndLoadSceneInternal());
    }

    IEnumerator fadeAndLoadSceneInternal()
    {
        AllowInput = false;
        fading = true;
        float startTime = Time.time;
        canvasGroup.alpha = 0f;
        canvas.enabled = true;
        while (Time.time - startTime < fadeOutTime)
        {
            canvasGroup.alpha = Mathf.Lerp(0f, 1f, (Time.time - startTime) / fadeOutTime);
            yield return null;
        }
        canvasGroup.alpha = 1f;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
