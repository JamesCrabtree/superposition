using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Colors;

public class Portal : MonoBehaviour
{
    public Portal pairedPortal;

    [HideInInspector]
    public bool waitToReenter = false;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(waitToReenter)
        {
            waitToReenter = false;
            return;
        }

        if(collision.gameObject.CompareTag("Player") && pairedPortal != null)
        {
            pairedPortal.waitToReenter = true;
            collision.gameObject.transform.position = pairedPortal.transform.position;
        }
            
    }
}
