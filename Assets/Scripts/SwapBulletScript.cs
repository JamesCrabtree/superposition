using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapBulletScript : MonoBehaviour
{
    public enum ColorType
    { 
        Pink,
        Blue
    }

    public ColorType BulletColor;

    public GameObject BulletContactParticlePrefab;

    public bool InDestroyRoutine { get; private set; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (InDestroyRoutine)
            return;

        if (ShootController.Instance.HoldingBullet)
            return;

        if (collision.tag.Equals("Swappable"))
        {
            Swappable swappable = collision.GetComponent<Swappable>();
            if (swappable == null)
                swappable = collision.GetComponentInParent<Swappable>();

            if (swappable != null)
            {
                StartCoroutine(playContactParticlesAndSoundThenDestroy(collision.transform, swappable, true, true));
            }
        }
        else if (collision.tag.Equals("Wall") || collision.tag.Equals("Water"))
        {
            StartCoroutine(playContactParticlesAndSoundThenDestroy(collision.transform, null, false, true));
        }
        else
        {
            return;
        }
    }

    public void DestroyBeforeContact()
    {
        StartCoroutine(playContactParticlesAndSoundThenDestroy(null, null, false, false));
    }

    IEnumerator playContactParticlesAndSoundThenDestroy(Transform other, Swappable swappableObject, bool swappable, bool contact)
    {
        if (InDestroyRoutine)
            yield break;

        InDestroyRoutine = true;

        ParticleSystem particleSystem = GetComponent<ParticleSystem>();
        if (particleSystem != null)
        {
            particleSystem.loop = false;
            particleSystem.Stop();
        }


        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            if (swappable && other != null)
            {
                //yield return new WaitForFixedUpdate();
                float dist = Vector2.Distance(transform.position, other.position) * 0.5f;
                float timeWait = dist / rb.velocity.magnitude;
                yield return new WaitForSeconds(timeWait/2);

                if (contact)
                {
                    if (BulletContactParticlePrefab != null)
                    {
                        GameObject spawnedParticle = Instantiate(BulletContactParticlePrefab, other.transform.position, Quaternion.identity);
                    }
                    GunHitSounds.Instance.PlayRandomSound();
                    CameraShake.Instance.Shake(CameraShake.ShakeType.Basic);
                }

                yield return new WaitForSeconds(timeWait/2);
                rb.simulated = false;
            }
        }

        AudioSource holdSound = GetComponent<AudioSource>();
        if (holdSound != null)
        {
            holdSound.Stop();
            holdSound.enabled = false;
        }


        Collider2D collider = GetComponent<Collider2D>();
        if (collider != null)
            collider.enabled = false;

        if (contact)
        {
            yield return new WaitForSeconds(0.6f);
            SwapController.Instance.AssignObjectAndSwap(swappableObject, BulletColor);
        }

        Destroy(gameObject, 1f);
    }
}
