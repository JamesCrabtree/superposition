using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraDirections;

namespace CameraDirections
{
    public enum Directions
    {
        North,
        South,
        East,
        West,
        Middle
    }
}

public class CameraManager : MonoBehaviour
{
    [Range(0f, 5f)]
    public float CameraZoomOutExtra = 0f;

    private const float moveDistance = 20f;
    private const float zoomOutSpeed = 15f;
    //private const float zoomInSpeed = 0.2f;
    private const float zoomInSpeed = 20f;
    public const float MinZoomSize = 15f;
    public const float MaxZoomSize = 15f;

    public static float curZoomSize = MinZoomSize;
    public static bool atPosition;

    PlayerController playerController;
    CameraShake CameraShake;
    Camera Camera;

    private float moveSpeed = 100f;
    private float targetZoomSize;

    private bool zooming;
    private float cameraDefaultZoomSize;
    public bool atStartPos
    {
        get
        {
            return !zooming;
        }
    }

    public static void setDirection(Directions dir)
    {
        atPosition = false;
    }

    private Vector3 getPlayerPosition()
    {
        Vector3 playerPos = PlayerController.Player.transform.position;
        return new Vector3(playerPos.x, playerPos.y, -10);
    }

    public Bounds OrthographicBounds()
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = Camera.orthographicSize * 2f;
        Bounds bounds = new Bounds(
            Camera.transform.position,
            new Vector3(cameraHeight * screenAspect, cameraHeight, 0f));
        return bounds;
    }

    public static CameraManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        transform.position = getPlayerPosition();
        playerController = PlayerController.Instance;
        Camera = GetComponent<Camera>();
        CameraShake = GetComponent<CameraShake>();
        //Camera.orthographicSize = curZoomSize;
        //cameraDefaultZoomSize = Camera.orthographicSize;
        //zooming = false;
    }

    void LateUpdate()
    {
        //curZoomSize = Camera.orthographicSize;
        //float zoomSpeed = targetZoomSize > curZoomSize ? zoomOutSpeed : zoomInSpeed;
        //Camera.orthographicSize = Mathf.MoveTowards(curZoomSize, targetZoomSize, Time.unscaledDeltaTime * zoomSpeed);
        transform.position = Vector3.MoveTowards(transform.position, getPlayerPosition(), Time.deltaTime * moveSpeed);
    }
}