using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Colors
{
    public enum ObjectColor
    {
        Red,
        Green,
        Blue,
        Yellow
    }
}