using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour
{
    public GameObject PinkBulletPrefab, BlueBulletPrefab;

    public static ShootController Instance;

    private AimController aimController;

    public bool InShootRoutine { get; private set; }
    public bool HoldingBullet { get; private set; }
    public bool shootPinkTrueBlueFalse { get; set; }

    private const float minVelocity = 3.5f;
    private const float maxVelocity = 7f;
    private const float timeGap = 1f;

    private const float bulletHoldSpeed = 10f;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        aimController = AimController.Instance;
        shootPinkTrueBlueFalse = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && !InShootRoutine)
        {
            StartCoroutine(shootRoutine());
        }
    }

    IEnumerator shootRoutine()
    {
        InShootRoutine = true;
        HoldingBullet = true;
        Pivot.Dimension startDimension = Pivot.curDimension;

        GameObject spawnedBullet = Instantiate(shootPinkTrueBlueFalse ? PinkBulletPrefab : BlueBulletPrefab, aimController.BulletSpawnPoint(), Quaternion.identity);
        yield return null;
        SwapBulletScript bulletScript = spawnedBullet.GetComponent<SwapBulletScript>();
        float holdStartTime = Time.time;
        while (Input.GetKey(KeyCode.Mouse0))
        {
            if (Pivot.curDimension != startDimension)
            {
                bulletScript.DestroyBeforeContact();
                InShootRoutine = false;
                HoldingBullet = false;
                yield break;
            }

            spawnedBullet.transform.position = Vector2.MoveTowards(spawnedBullet.transform.position, aimController.BulletSpawnPoint(), Time.deltaTime * bulletHoldSpeed);
            yield return null;
        }

        CameraShake.Instance.Shake(CameraShake.ShakeType.Basic, multiplier: 0.35f);
        float speed = Mathf.Lerp(minVelocity, maxVelocity, ((Time.time - holdStartTime) / timeGap));
        Rigidbody2D rb = spawnedBullet.GetComponent<Rigidbody2D>();
        rb.velocity = aimController.AimDirection() * speed;
        GunShootSounds.Instance.PlayRandomSound();
        HoldingBullet = false;

        yield return new WaitForSeconds(0.2f);

        yield return new WaitUntil(() => bulletScript != null && bulletScript.InDestroyRoutine || Pivot.curDimension != startDimension);
        if (Pivot.curDimension != startDimension)
        {
            bulletScript.DestroyBeforeContact();
            InShootRoutine = false;
            yield break;
        }

        if (shootPinkTrueBlueFalse)
        {
            yield return new WaitUntil(() => SwapController.Instance.PinkOwned != null || spawnedBullet == null || Pivot.curDimension != startDimension);

            if (Pivot.curDimension != startDimension)
            {
                bulletScript.DestroyBeforeContact();
            }
            else
            {
                shootPinkTrueBlueFalse = false;
            }
        }
        else
        {
            yield return new WaitUntil(() => SwapController.Instance.BlueOwned != null || spawnedBullet == null || Pivot.curDimension != startDimension);

            if (Pivot.curDimension != startDimension)
            {
                bulletScript.DestroyBeforeContact();
            }
            else
            {
                shootPinkTrueBlueFalse = true;
            }
        }

        InShootRoutine = false;
    }

}
