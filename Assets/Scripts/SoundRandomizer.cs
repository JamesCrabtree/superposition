using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundRandomizer : MonoBehaviour
{
    public List<AudioClip> sounds;

    private AudioSource audioSource;

    [Range(0f, 1f)] public float maxVolume, minVolume;
    [Range(0f, 10f)] public float maxPitch, minPitch;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayRandomSound()
    {
        AudioClip chosenSound = sounds[Random.Range(0, sounds.Count)];
        audioSource.clip = chosenSound;
        audioSource.pitch = Random.Range(minPitch, maxPitch);
        audioSource.volume = Random.Range(minVolume, maxVolume);
        audioSource.Play();
    }

}
