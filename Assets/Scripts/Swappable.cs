using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swappable : MonoBehaviour
{
    private Swappable replicatedSwappable;
    private Rigidbody2D rb;

    public bool InSwap { get; private set; }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Replicated replicated = GetComponent<Replicated>();
        if (replicated != null && replicated.isReplicated())
            replicatedSwappable = replicated.getOtherSwappable();
    }

    public void swap(Swappable swappable)
    {
        singleDimensionSwap(swappable);
        Replicated swappableReplicated = swappable.GetComponent<Replicated>();
        if(swappableReplicated != null && swappableReplicated.isReplicated())
            swap(replicatedSwappable, swappableReplicated.getOtherSwappable());
    }

    public void singleDimensionSwap(Swappable swappable)
    {
        StartCoroutine(singleDimensionSwapRoutine(swappable));
    }

    private IEnumerator singleDimensionSwapRoutine(Swappable swappable)
    {
        InSwap = true;
        disableRb();
        swappable.disableRb();
        yield return null;
        Vector2 currentPos = transform.position;
        transform.position = swappable.transform.position;
        swappable.transform.position = currentPos;
        yield return null;
        enableRb();
        swappable.enableRb();
        InSwap = false;
    }

    public void disableRb()
    {
        rb.velocity = Vector2.zero;
        rb.inertia = 0f;
        rb.simulated = false;
    }

    public void enableRb()
    {
        rb.simulated = true;
    }

    private static void swap(Swappable swappable1, Swappable swappable2)
    {
        swappable1.singleDimensionSwap(swappable2);
    }
}
