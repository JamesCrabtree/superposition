using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Curves;
using Colors;

public class PressurePlate : MonoBehaviour
{
    public ObjectColor Color;
    //public List<DoorLockedBySwitchOrPlate> doorsDependent;
    //public List<TeslaCoil> teslaCoilsDependent;

    public bool PressedDown { get; private set; }
    private float lastTimePressedDown;
    private int instanceID;

    Animator anim;
    AudioSource pressSound;
    public GameObject lightObject;
    public Door door;
    public LevelChange levelChange;

    private const float notPressingDownOnPlateTime = 0.5f;

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        pressSound = GetComponentInChildren<AudioSource>();
        PressedDown = false;
        instanceID = GetInstanceID();
        UpdateAnimations();
        lightObject.SetActive(false);
    }

    private void UpdateAnimations()
    {
        string animationName = "";
        if (Color == ObjectColor.Blue)
            animationName += "Blue";
        else if (Color == ObjectColor.Green)
            animationName += "Green";
        else if (Color == ObjectColor.Yellow)
            animationName += "Yellow";
        else if (Color == ObjectColor.Red)
            animationName += "Red";

        animationName += (PressedDown ? " On" : " off");
        //Debug.Log("Animation name: " + animationName);
        anim.Play(animationName);
    }

    private void LateUpdate()
    {
        if (Time.fixedTime - lastTimePressedDown > notPressingDownOnPlateTime)
        {
            PressedDown = false;
            lightObject.SetActive(false);
            UpdateAnimations();
            //foreach (DoorLockedBySwitchOrPlate door in doorsDependent)
            //{
            //    if (door != null)
            //        door.AlterByPressurePlate(false);
            //}
            //foreach (TeslaCoil coil in teslaCoilsDependent)
            //{
            //    if (coil != null)
            //        coil.AlterByPressurePlate(false, instanceID);
            //}
            this.GetInstanceID();
        }
    }

    private void handleSwitchEffects()
    {
        if (door != null)
        {
            door.switchState();
        }

        if (levelChange != null)
        {
            levelChange.open();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Crate"))
        {
            Crate crate = collision.GetComponentInParent<Crate>();
            if (crate != null && crate.Color == Color)
            {
                handleSwitchEffects();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("Crate"))
        {
            Crate crate = collision.GetComponentInParent<Crate>();
            if (crate != null && crate.Color == Color)
            {
                handleSwitchEffects();
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag.Equals("Crate"))
        {
            Crate crate = collision.GetComponentInParent<Crate>();
            if (crate != null && crate.Color == Color)
            {
                if (!PressedDown)
                    pressSound.Play();

                Debug.Log("Pressure Plate Pressed Down");
                lastTimePressedDown = Time.fixedTime;
                PressedDown = true;
                UpdateAnimations();
                lightObject.SetActive(true);
                //foreach (DoorLockedBySwitchOrPlate door in doorsDependent)
                //{
                //    if (door != null)
                //        door.AlterByPressurePlate(true);
                //}
                //foreach (TeslaCoil coil in teslaCoilsDependent)
                //{
                //    if (coil != null)
                //        coil.AlterByPressurePlate(true, instanceID);
                //}
            }
        }
    }
}