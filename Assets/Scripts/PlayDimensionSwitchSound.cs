using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayDimensionSwitchSound : MonoBehaviour
{
    private AudioSource sound;

    public static PlayDimensionSwitchSound Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        sound = GetComponent<AudioSource>();    
    }

    public void PlaySound()
    {
        sound.volume = Random.Range(0.95f, 1.0f);
        sound.pitch = Random.Range(0.96f, 1.14f);
        sound.Play();
    }
}
