using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusicStarter : MonoBehaviour
{
    public AudioClip marsMusic, lushMusic;
    private AudioSource audioSource;
    public static BackgroundMusicStarter Instance;
    private bool playingMusic;


    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (playingMusic)
            Destroy(this.gameObject);
        Instance = this;
        playingMusic = true;
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        SwitchMusic(Pivot.curDimension);

    }

    public void SwitchMusic(Pivot.Dimension newDimension)
    {
        float savedTime = audioSource.time;
        if (audioSource.isPlaying)
            audioSource.Pause();

        if (newDimension == Pivot.Dimension.Mars)
            audioSource.clip = marsMusic;
        else
            audioSource.clip = lushMusic;

        audioSource.time = savedTime;
        audioSource.Play();
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.M))
        //    paused = !paused;

        //if (paused)
        //{
        //    foreach (AudioSource music in musicList)
        //    {
        //        if (music == null)
        //            continue;

        //        if (music.isPlaying)
        //            music.Pause();
        //    }
        //}
    }

    //IEnumerator playAllMusic()
    //{
    //    playingMusic = true;
    //    while (true)
    //    {
    //        foreach (AudioSource music in musicList)
    //        {
    //            if (music == null)
    //                continue;

    //            yield return new WaitWhile(() => paused);

    //            music.volume = soundVolume;
    //            music.Play();
    //            yield return new WaitUntil(() => music.isPlaying);
    //            yield return new WaitUntil(() => !music.isPlaying);
    //            yield return new WaitForSeconds(2f);
    //        }
    //        yield return null;
    //    }
    //}
}