using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pivot : MonoBehaviour
{
    public enum Dimension
    { 
        Mars,
        Lush
    }

    public static bool InSwitchDimensionsRoutine { get; private set; }

    public Dimension WhichDimension;

    public static Transform MarsDimensionPivotTransform, LushDimensionPivotTransform;
    public static Pivot MarsDimensionPivot, LushDimensionPivot;
    public static Dimension curDimension;

    public static Pivot CurDimensionPivot()
    {
        return curDimension == Dimension.Mars ? MarsDimensionPivot : LushDimensionPivot;
    }

    void Awake()
    {
        if (WhichDimension == Dimension.Mars)
        {
            MarsDimensionPivotTransform = transform;
            MarsDimensionPivot = this;
        }
        else
        {
            LushDimensionPivotTransform = transform;
            LushDimensionPivot = this;
        }
    }

    public static Vector2 positionRelativeToCurrentDimension(Vector2 pos)
    {
        if (curDimension == Dimension.Mars)
            return pos - (Vector2)MarsDimensionPivotTransform.position;
        else
            return pos - (Vector2)LushDimensionPivotTransform.position;
    }

    public static Dimension otherDimension()
    {
        return curDimension == Dimension.Mars ? Dimension.Lush : Dimension.Mars;
    }

    public static Transform otherDimensionPivot()
    {
        return curDimension == Dimension.Mars ? LushDimensionPivotTransform : MarsDimensionPivotTransform;
    }

    public static Vector2 rawPosInOtherDimension(Vector2 pos)
    {
        return (Vector2)otherDimensionPivot().position + positionRelativeToCurrentDimension(pos);
    }

    public void SwitchDimensions()
    {
        StartCoroutine(switchDimensionRoutine());
    }

    IEnumerator switchDimensionRoutine()
    {
        if (InSwitchDimensionsRoutine)
            yield break;

        InSwitchDimensionsRoutine = true;
        PlayDimensionSwitchSound.Instance.PlaySound();
        yield return new WaitForSeconds(0.3f);
        PlayerController.Player.transform.position = rawPosInOtherDimension(PlayerController.Player.transform.position);
        curDimension = otherDimension();
        SwapController.Instance.PinkOwned = null;
        SwapController.Instance.BlueOwned = null;
        ShootController.Instance.shootPinkTrueBlueFalse = true;
        BackgroundMusicStarter.Instance.SwitchMusic(curDimension);
        InSwitchDimensionsRoutine = false;
    }

}
