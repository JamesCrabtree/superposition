using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimController : MonoBehaviour
{
    private const float spawnDist = 1f;

    public static AimController Instance;

    private void Awake()
    {
        Instance = this;
    }

    public Vector3 AimDirection()
    {
        Vector3 shootDirection = Input.mousePosition;
        shootDirection = Camera.main.ScreenToWorldPoint(shootDirection);
        shootDirection = shootDirection - transform.position;

        shootDirection.z = 0.0f;
        shootDirection = shootDirection.normalized;

        return shootDirection;
    }

    public Vector2 BulletSpawnPoint()
    {
        return transform.position + (spawnDist * AimDirection());
    }
}
