using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundDistanceAttenuationScript : MonoBehaviour
{
    public float distanceWhereMaxVolume;
    public float fadeOffDistance;
    public float maxVolume;
    public float minVolume;
    public float maxRandomPitch;
    public float minRandomPitch;

    public AudioSource audioSource;
    private Transform player;

    void Start()
    {
        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();
        player = PlayerController.Player.transform;

        audioSource.pitch = Random.Range(minRandomPitch, maxRandomPitch);
    }

    public static float volumeBasedOnDistanceToPlayer(Vector3 position, float maxVol, float minVol, float distanceWhereMaxVolumePlays, float distanceVolumeFadesOff)
    {
        float distance = Vector2.Distance(position, PlayerController.Player != null ? PlayerController.Player.transform.position : position);
        if (distance <= distanceWhereMaxVolumePlays)
        {
            return maxVol;
        }
        else
        {
            float proximity = 1f - ((distance - distanceWhereMaxVolumePlays) / distanceVolumeFadesOff);
            return Mathf.Lerp(minVol, maxVol, proximity);
        }
    }

    void Update()
    {
        float distance = Vector2.Distance(player.position, transform.position);
        if (distance <= distanceWhereMaxVolume)
        {
            audioSource.volume = maxVolume;
        }
        else
        {
            float proximity = 1f - ((distance - distanceWhereMaxVolume) / fadeOffDistance);
            audioSource.volume = Mathf.Lerp(minVolume, maxVolume, proximity);
        }
    }
}
