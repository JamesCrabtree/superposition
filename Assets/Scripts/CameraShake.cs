using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{

    public enum ShakeType
    {
        Basic
    }

    bool shaking;
    bool startingShake;
    bool waiting;
    private const float basicMagnitude = 0.06f;
    private const float basicDuration = 0.2f;

    private const float cameraShakeSizeFactorMultiplier = CameraManager.MinZoomSize / 7f; //when we change the min camera size, scale the camera shake accordingly. (e.g. more shake needed for more zoomed out camera)

    Coroutine savedCoroutine;
    ShakeType prevType = ShakeType.Basic;

    private const bool boomerangShakeEnabled = true;

    public static CameraShake Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        prevType = ShakeType.Basic;
        waiting = false;
        shaking = false;
    }

    public void Shake(ShakeType type = ShakeType.Basic, float multiplier = 1f, float durationMultiplier = 1f)
    {
        if (multiplier <= 0f)
            return;

        prevType = type;
        startingShake = true;
        if (savedCoroutine != null)
            StopCoroutine(savedCoroutine);
        savedCoroutine = StartCoroutine(shakeInternal(type, multiplier, durationMultiplier));
        shaking = true;
        startingShake = false;
    }

    public bool Shaking
    {
        get { return shaking || startingShake; }
    }

    private float decideStartingMagnitude(float elapsed, float dur, ShakeType type)
    {
        return basicMagnitude;
    }

    private float magnitudeOverTime(float elapsed, float dur, ShakeType type, float max)
    {
        return max * Mathf.Pow(1f - (elapsed / dur), 0.7f);
    }

    public bool PrevShakeDueTo(ShakeType type)
    {
        return prevType.Equals(type);
    }

    private float decideDuration(ShakeType type)
    {
        return basicDuration;
    }
    private IEnumerator shakeInternal(ShakeType type, float multiplier, float durationMultiplier)
    {
        float startTime = Time.time;
        float duration = decideDuration(type) * durationMultiplier;
        float startMagnitude = decideStartingMagnitude(Time.time - startTime, duration, type);
        float m = startMagnitude;
        float y = Random.Range(-1f, 1f) * m;
        float x = Random.Range(-1f, 1f) * m;
        Vector3 origPosition = transform.position;
        Vector3 shakeOffset = Vector3.zero;
        Quaternion targetRotation = Quaternion.identity;
        while (Time.time - startTime < duration && !waiting)
        {
            if (!PlayerController.Instance.Alive)
                break;

            shakeOffset = new Vector2(x, y);
            float xNeg = x / Mathf.Abs(x);
            float yNeg = y / Mathf.Abs(y);
            m = magnitudeOverTime(Time.time - startTime, duration, type, startMagnitude) * multiplier * cameraShakeSizeFactorMultiplier;
            x = xNeg;
            y = yNeg;
            shakeOffset = Random.insideUnitCircle * new Vector2(x, y) * m;
            if (shakeOffset.magnitude > 0f)
                transform.rotation = Quaternion.LookRotation(Vector3.forward + (shakeOffset * 0.3f));
            else
                transform.rotation = Quaternion.Euler(Vector3.zero);
            yield return new WaitForEndOfFrame();
        }

        transform.rotation = Quaternion.LookRotation(Vector3.forward);

        shaking = false;
    }
}
